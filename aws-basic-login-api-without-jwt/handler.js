const serverless = require("serverless-http");
const express = require("express");
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();

var users = [
  {
    email: "sreejith.k@gmail.com",
    password: "1234"
  }
]

app.use(cors());
app.use(bodyParser.json());

app.get("/", (req, res, next) => {
  return res.status(200).json({
    message: "Hello from root!",
  });
});

app.post("/login", (req, res) => {
  let check = users.find(user => user.email === req.body.email);
  if (check) {
    if (check.password === req.body.password) {
      return res.status(200).json({
        message: "Successfull Login!",
      });
    } else {
      return res.status(200).json({
        message: "Incorrect Password",
      });
    }
  } else {
    return res.status(200).json({
      message: "User not Found!!",
    });
  }
});

app.get("/hello", (req, res, next) => {
  return res.status(200).json({
    message: "Hello from path!",
  });
});

app.use((req, res, next) => {
  return res.status(404).json({
    error: "Not Found",
  });
});

module.exports.handler = serverless(app);
